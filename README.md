﻿# Stock Prices

Encuentra el máximo beneficio que pudo haber sido realizado comprando un elemento a un precio y vendiéndolo a otro (comprar y vender una sola vez).

---

### Requerimientos

1. `Java v1.8`

### Tecnologías utilizadas

1. `Java`
2. `Maven` para el control / gestión de dependencias 

### Puesta en marcha
1. Clonar desde el repositorio
``` git
git clone https://gorellanacl@bitbucket.org/gorellanacl/sodimac-stockprices.git
```

2. Abrir la aplicación con cualquier IDE y __ejecutar las pruebas unitarias__ dispuestas. En Netbeans (IDE utilizado por mí) es con __CTRL+F6__ por ejemplo.