
import cl.sodimac.stockprices.StockPrice;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gasto
 */
public final class StockPriceTest {
    
    private StockPrice stockPrice;
    
    public StockPriceTest() {}
    
    @BeforeMethod
    public void setUp() {
        stockPrice = new StockPrice();
    }
    
    @Test
    public void calculateProfits() {
        assertEquals(stockPrice.maxProfit(new int[]{44, 30, 22, 32, 35, 30, 41, 38, 15}), 19);
        assertEquals(stockPrice.maxProfit(new int[]{2, 3, 4, 2}), 2);
        assertEquals(stockPrice.maxProfit(new int[]{50, 30, 4, 2}), -1);
        assertEquals(stockPrice.maxProfit(new int[]{2, 18, 1, 21}), 20);
        assertEquals(stockPrice.maxProfit(new int[]{1, 2, 3, 4, 5}), 4);
        assertEquals(stockPrice.maxProfit(new int[]{50, 50}), -1);
        assertEquals(stockPrice.maxProfit(new int[]{7}), -1);
    }
    
}
