/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.sodimac.stockprices;

/**
 *
 * @author gasto
 */
public class StockPrice {
    
    static final int NO_PROFIT = -1;
    
    /**
     * Calculate max profit between array values (buying once, selling once).
     * No profit will return -1. No profit means profit <= 0
     * 
     * @param arr   Array to be checked
     * @return int Max profit (if it applies) found 
     */
    public int maxProfit(int[] arr) {
        
        // Requires at least 2 elements
        int l = arr.length;
        if(l < 2) { return NO_PROFIT; }
        
        // If array is desc sorted, it should to return -1
        // Keep min value and max diff (profit)
        int minValue = arr[0];
        int profit = arr[1] - minValue;
        
        // Traveling array, calculating values
        for(int i=1; i<l; i++) {
            int val = arr[i];
            int potentialProfit = val - minValue;
            
            if(potentialProfit > profit) { profit = potentialProfit; }
            if(val < minValue) { minValue = val; }
        }
        
        return profit <= 0 ? NO_PROFIT : profit;
    }
    
}
